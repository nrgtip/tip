/*
 * org.nrg.tip.messaging.MockPacsSessionExportRequestListener
 * TIP is developed by the Neuroinformatics Research Group
 * XNAT http://www.xnat.org
 * Copyright (c) 2013, Washington University School of Medicine
 * All Rights Reserved
 *
 * Released under the Simplified BSD.
 *
 * Last modified 9/24/13 6:11 PM
 */

package org.nrg.tip.messaging;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class MockPacsSessionExportRequestListener {

    private final static Logger log = LoggerFactory.getLogger(MockPacsSessionExportRequestListener.class);

    public void onPacsSessionExportRequest(final PacsSessionExportRequest pacsSessionExportRequest) {
	log.info("Mock listener got request: " + pacsSessionExportRequest);
	PacsSessionExportRequestListenerTest.messageReceivedByListener = pacsSessionExportRequest;
    }
}
