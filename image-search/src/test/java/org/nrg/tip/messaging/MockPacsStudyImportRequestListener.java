/*
 * org.nrg.tip.messaging.MockPacsStudyImportRequestListener
 * TIP is developed by the Neuroinformatics Research Group
 * XNAT http://www.xnat.org
 * Copyright (c) 2013, Washington University School of Medicine
 * All Rights Reserved
 *
 * Released under the Simplified BSD.
 *
 * Last modified 9/24/13 6:11 PM
 */

package org.nrg.tip.messaging;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class MockPacsStudyImportRequestListener {

    private final static Logger log = LoggerFactory.getLogger(MockPacsStudyImportRequestListener.class);

    public void onPacsStudyImportRequest(final PacsStudyImportRequest pacsStudyImportRequest) {
	log.info("Mock listener got request: " + pacsStudyImportRequest);
	PacsStudyImportRequestListenerTest.messageReceivedByListener = pacsStudyImportRequest;
    }
}
