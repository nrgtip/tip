/*
 * org.nrg.tip.TipBaseTest
 * TIP is developed by the Neuroinformatics Research Group
 * XNAT http://www.xnat.org
 * Copyright (c) 2013, Washington University School of Medicine
 * All Rights Reserved
 *
 * Released under the Simplified BSD.
 *
 * Last modified 9/24/13 6:11 PM
 */

package org.nrg.tip;

import org.springframework.test.context.ContextConfiguration;

@ContextConfiguration(locations = "/org/nrg/tip/Tests-context.xml")
public abstract class TipBaseTest {
}
