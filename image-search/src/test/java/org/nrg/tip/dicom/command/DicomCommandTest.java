/*
 * org.nrg.tip.dicom.command.DicomCommandTest
 * TIP is developed by the Neuroinformatics Research Group
 * XNAT http://www.xnat.org
 * Copyright (c) 2013, Washington University School of Medicine
 * All Rights Reserved
 *
 * Released under the Simplified BSD.
 *
 * Last modified 9/24/13 6:11 PM
 */

package org.nrg.tip.dicom.command;

import javax.inject.Inject;

import org.junit.Before;
import org.nrg.tip.TipBaseTest;
import org.nrg.tip.dicom.command.cecho.dcm4che.tool.Dcm4cheToolCEchoSCU;
import org.nrg.tip.dicom.net.DicomConnectionProperties;
import org.nrg.tip.dicom.strategy.orm.OrmStrategy;

public abstract class DicomCommandTest extends TipBaseTest {

    @Inject
    protected DicomConnectionProperties dicomConnectionProperties;

    @Inject
    protected OrmStrategy ormStrategy;

    @Before
    public void before() {
	try {
	    new Dcm4cheToolCEchoSCU(dicomConnectionProperties).cecho();
	} catch (RuntimeException e) {
	    throw e;
	} catch (Exception e) {
	    throw new RuntimeException("Unable to ping test PACS." + "\nTo run these unit tests:"
		    + "\n\t1) Configure the PACS connection properties appropriately via Spring."
		    + "\n\t2) Ensure the PACS is listening on the specified port.");
	}
    }
}
