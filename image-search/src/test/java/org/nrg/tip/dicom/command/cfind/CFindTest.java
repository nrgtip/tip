/*
 * org.nrg.tip.dicom.command.cfind.CFindTest
 * TIP is developed by the Neuroinformatics Research Group
 * XNAT http://www.xnat.org
 * Copyright (c) 2013, Washington University School of Medicine
 * All Rights Reserved
 *
 * Released under the Simplified BSD.
 *
 * Last modified 9/24/13 6:11 PM
 */

package org.nrg.tip.dicom.command.cfind;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.nrg.tip.dicom.command.DicomCommandTest;
import org.nrg.tip.dicom.command.cfind.dcm4che.tool.Dcm4cheToolCFindSCU;
import org.nrg.tip.dicom.strategy.orm.ResultSetLimitStrategy;
import org.nrg.tip.dicom.strategy.orm.mcds.MedicalConnectionsDicomServerResultSetLimitStrategy;
import org.nrg.tip.domain.Patient;
import org.nrg.tip.domain.PatientName;
import org.nrg.tip.domain.ReferringPhysicianName;
import org.nrg.tip.domain.Series;
import org.nrg.tip.domain.Study;
import org.nrg.tip.dto.PacsSearchCriteria;
import org.nrg.tip.dto.PacsSearchResults;
import org.nrg.xnat.utils.DateRange;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
public class CFindTest extends DicomCommandTest {

    private final static DateFormat TEST_DATE_FORMAT = new SimpleDateFormat("yyyy-MM-dd");

    private CFindSCU cfindSCU;

    private ResultSetLimitStrategy savedResultSetLimitStrategy;

    @Before
    public void before() {
        cfindSCU = new Dcm4cheToolCFindSCU(dicomConnectionProperties, ormStrategy);
        savedResultSetLimitStrategy = ((Dcm4cheToolCFindSCU) cfindSCU).getOrmStrategy().getResultSetLimitStrategy();
    }

    @After
    public void after() {
        ((Dcm4cheToolCFindSCU) cfindSCU).getOrmStrategy().setResultSetLimitStrategy(savedResultSetLimitStrategy);
    }

    @Test
    public void cfindPatientById() {
        final Patient actual = cfindSCU.cfindPatientById("29572");
        compareExpectedPatientToActual(actual);
    }

    @Test
    public void cfindPatientByIdNotFound() {
        final Patient actual = cfindSCU.cfindPatientById("29573");
        assertNull(actual);
    }

    @Test
    public void cfindPatientByLastName() {
        final PacsSearchCriteria searchCriteria = new PacsSearchCriteria();
        searchCriteria.setPatientName("Cartman");

        final PacsSearchResults<String, Patient> results = cfindSCU.cfindPatientsByExample(searchCriteria);
        compareExpectedPatientsToActual(results);
    }

    @Test
    public void cfindPatientByLastNameNotFound() {
        final PacsSearchCriteria searchCriteria = new PacsSearchCriteria();
        searchCriteria.setPatientName("SDFSDFSDFS");

        final PacsSearchResults<String, Patient> results = cfindSCU.cfindPatientsByExample(searchCriteria);
        assertEquals(0, results.getResults().size());
    }

    @Test
    public void cfindPatientByExample() {
        final PacsSearchCriteria searchCriteria = new PacsSearchCriteria();
        searchCriteria.setPatientId("29572");
        searchCriteria.setPatientName("Cartman, Eric");

        final PacsSearchResults<String, Patient> results = cfindSCU.cfindPatientsByExample(searchCriteria);
        compareExpectedPatientsToActual(results);
    }

    @Test
    public void cfindPatientByLastNameAndPartialFirstname() {
        final PacsSearchCriteria searchCriteria = new PacsSearchCriteria();
        searchCriteria.setPatientName("Cartman, E");

        final PacsSearchResults<String, Patient> results = cfindSCU.cfindPatientsByExample(searchCriteria);
        compareExpectedPatientsToActual(results);
    }

    @Test(expected = SearchCriteriaTooVagueException.class)
    public void cfindPatientsEmptySearchCriteria() {
        // the strategy for our own PACS will not throw this exception, so to test inject a different strategy
        ((Dcm4cheToolCFindSCU) cfindSCU).getOrmStrategy().setResultSetLimitStrategy(
                new MedicalConnectionsDicomServerResultSetLimitStrategy());
        cfindSCU.cfindPatientsByExample(new PacsSearchCriteria());
    }

    @Test(expected = SearchCriteriaTooVagueException.class)
    public void cfindPatientEmptyPatientId() {
        cfindSCU.cfindPatientById(null);
    }

    private void compareExpectedPatientsToActual(PacsSearchResults<String, Patient> actual) {
        assertEquals(1, actual.getResults().size());
        assertEquals(false, actual.hasLimitedResultSetSize());
    }

    private void compareExpectedPatientToActual(Patient actual) {
        final Patient expected = expectedPatient();
        assertEquals(expected, actual);
    }

    private Patient expectedPatient() {
        return mockPatient();
    }

    public static Patient mockPatient() {
        final Patient p = new Patient();
        p.setId("29572");
        p.setName(new PatientName("Eric", "Cartman"));
        try {
            p.setBirthDate(TEST_DATE_FORMAT.parse("1990-01-01"));
        } catch (ParseException e) {
            throw new RuntimeException(e);
        }
        p.setSex("M");
        return p;
    }

    @Test
    public void cfindStudiesByExample() throws ParseException {
        final PacsSearchCriteria searchCriteria = new PacsSearchCriteria();
        searchCriteria.setPatientId("29572");
        searchCriteria.setAccessionNumber("S1");
        searchCriteria.setStudyDateRange(new DateRange(TEST_DATE_FORMAT.parse("2010-12-19"), TEST_DATE_FORMAT
                .parse("2010-12-19")));

        final PacsSearchResults<String, Study> results = cfindSCU.cfindStudiesByExample(searchCriteria);
        compareExpectedStudiesToActual(results);
    }

    @Test
    public void cfindStudiesByOpenEndedStartDateRange() throws ParseException {
        final PacsSearchCriteria searchCriteria = new PacsSearchCriteria();
        searchCriteria.setStudyDateRange(new DateRange(null, TEST_DATE_FORMAT.parse("2010-12-19")));

        final PacsSearchResults<String, Study> results = cfindSCU.cfindStudiesByExample(searchCriteria);
        assertTrue(results.getResults().size() >= 1);
    }

    @Test
    public void cfindStudiesByOpenEndedEndDateRange() throws ParseException {
        final PacsSearchCriteria searchCriteria = new PacsSearchCriteria();
        searchCriteria.setStudyDateRange(new DateRange(TEST_DATE_FORMAT.parse("2010-12-19"), null));

        final PacsSearchResults<String, Study> results = cfindSCU.cfindStudiesByExample(searchCriteria);
        assertTrue(results.getResults().size() >= 1);
    }

    @Test
    public void cfindStudiesByExamplePatientIdNotFound() {
        final PacsSearchCriteria searchCriteria = new PacsSearchCriteria();
        searchCriteria.setPatientId("34325425");

        final PacsSearchResults<String, Study> results = cfindSCU.cfindStudiesByExample(searchCriteria);
        assertEquals(0, results.getResults().size());
    }

    @Test
    public void cfindStudiesByStudyDateNotFound() throws ParseException {
        final PacsSearchCriteria searchCriteria = new PacsSearchCriteria();
        Calendar cal = Calendar.getInstance();
        cal.add(Calendar.DATE, 1);
        Date tomorrow = cal.getTime();
        searchCriteria.setStudyDateRange(new DateRange(tomorrow, null));

        final PacsSearchResults<String, Study> results = cfindSCU.cfindStudiesByExample(searchCriteria);
        assertEquals(0, results.getResults().size());
    }

    @Test
    public void cfindStudyByIdNotFound() {
        final Study actual = cfindSCU.cfindStudyById("29573");
        assertNull(actual);
    }

    @Test
    public void cfindStudyById() {
        final Study actual = cfindSCU.cfindStudyById("1.2.840.113654.2.45.2.108105");
        compareExpectedStudyToActual(actual);
    }

    @Test(expected = SearchCriteriaTooVagueException.class)
    public void cfindStudiesEmptySearchCriteria() {
        // the strategy for our own PACS will not throw this exception, so to test inject a different strategy
        ((Dcm4cheToolCFindSCU) cfindSCU).getOrmStrategy().setResultSetLimitStrategy(
                new MedicalConnectionsDicomServerResultSetLimitStrategy());
        cfindSCU.cfindStudiesByExample(new PacsSearchCriteria());
    }

    @Test(expected = SearchCriteriaTooVagueException.class)
    public void cfindStudyEmptyStudyId() {
        cfindSCU.cfindStudyById(null);
    }

    private void compareExpectedStudiesToActual(PacsSearchResults<String, Study> actual) {
        assertEquals(1, actual.getResults().size());
        assertEquals(false, actual.hasLimitedResultSetSize());
    }

    private void compareExpectedStudyToActual(Study actual) {
        final Study expected = expectedStudy();
        assertEquals(expected, actual);
    }

    private Study expectedStudy() {
        return mockStudy();
    }

    public static Study mockStudy() {
        final Study s = new Study();
        s.setPatient(mockPatient());
        s.setStudyInstanceUid("1.2.840.113654.2.45.2.108105");
        try {
            s.setStudyDate(TEST_DATE_FORMAT.parse("2010-12-19"));
        } catch (ParseException e) {
            throw new RuntimeException(e);
        }
        s.setReferringPhysicianName(new ReferringPhysicianName("Jim", "Henry"));
        s.setStudyId("MR1");
        s.setAccessionNumber("S1");
        s.setStudyDescription("South Park brain study");
        return s;
    }

    @Test
    public void cfindSeriesByStudy() {
        final PacsSearchResults<String, Series> results = cfindSCU.cfindSeriesByStudy(expectedStudy());
        compareExpectedSeriesToActual(results);
    }

    @Test
    public void cfindSeriesByStudyNotFound() {
        final Study s = expectedStudy();
        s.setStudyInstanceUid("blahasdfasfgs");
        final PacsSearchResults<String, Series> results = cfindSCU.cfindSeriesByStudy(s);

        assertEquals(0, results.getResults().size());
    }

    @Test
    public void cfindSeriesById() {
        final Series actual = cfindSCU.cfindSeriesById("1.2.840.113654.2.45.2.115982");
        assertNotNull(actual);
        assertNotNull(actual.getStudy());
        assertEquals("1.2.840.113654.2.45.2.108105", actual.getStudy().getStudyInstanceUid());
        compareExpectedSeriesToActual(actual);
    }

    @Test
    public void cfindSeriesByIdNotFound() {
        final Series actual = cfindSCU.cfindSeriesById("342534tfsfasf243r23");
        assertNull(actual);
    }

    @Test(expected = SearchCriteriaTooVagueException.class)
    public void cfindSeriesEmptyStudy() {
        cfindSCU.cfindSeriesByStudy(null);
    }

    @Test(expected = SearchCriteriaTooVagueException.class)
    public void cfindSeriesEmptySeriesId() {
        cfindSCU.cfindSeriesById(null);
    }

    private void compareExpectedSeriesToActual(PacsSearchResults<String, Series> actual) {
        assertEquals(10, actual.getResults().size());
        assertEquals(false, actual.hasLimitedResultSetSize());
    }

    private void compareExpectedSeriesToActual(Series actual) {
        final Series expected = expectedSeries();
        // the series level doesn't bring back keys from all the way up the hierarchy
        // (it stops at the study UID)
        // so equalize the study objects before comparing
        expected.setStudy(actual.getStudy());
        assertEquals(expected, actual);
    }

    private Series expectedSeries() {
        return mockSeries();
    }

    public static Series mockSeries() {
        final Series s = new Series();
        s.setStudy(mockStudy());
        s.setSeriesInstanceUid("1.2.840.113654.2.45.2.115982");
        s.setSeriesNumber(4);
        s.setModality("MR");
        s.setSeriesDescription("SAG LOCALIZER");
        return s;
    }
}
