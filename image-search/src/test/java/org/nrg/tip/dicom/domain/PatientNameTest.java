/*
 * org.nrg.tip.dicom.domain.PatientNameTest
 * TIP is developed by the Neuroinformatics Research Group
 * XNAT http://www.xnat.org
 * Copyright (c) 2013, Washington University School of Medicine
 * All Rights Reserved
 *
 * Released under the Simplified BSD.
 *
 * Last modified 9/24/13 6:11 PM
 */

package org.nrg.tip.dicom.domain;

import static org.junit.Assert.assertNotNull;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;
import org.nrg.tip.domain.PatientName;

@RunWith(JUnit4.class)
public class PatientNameTest {

    /**
     * The constructor that takes a comma-delimited name is tested pretty well via the PatientNameStrategy class, so
     * we'll cheat here and just fill in the gaps
     */

    @Test
    public void constructorWithCommaDelimitedNameNull() {
	PatientName pn = new PatientName("");
	assertNotNull(pn);
    }
}
