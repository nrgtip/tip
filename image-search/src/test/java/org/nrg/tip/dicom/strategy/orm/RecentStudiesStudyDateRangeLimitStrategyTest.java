/*
 * org.nrg.tip.dicom.strategy.orm.RecentStudiesStudyDateRangeLimitStrategyTest
 * TIP is developed by the Neuroinformatics Research Group
 * XNAT http://www.xnat.org
 * Copyright (c) 2013, Washington University School of Medicine
 * All Rights Reserved
 *
 * Released under the Simplified BSD.
 *
 * Last modified 9/24/13 6:11 PM
 */

package org.nrg.tip.dicom.strategy.orm;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.util.Calendar;
import java.util.Date;

import org.apache.commons.lang.time.DateUtils;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;
import org.nrg.tip.dto.PacsSearchCriteria;
import org.nrg.tip.dto.StudyDateRangeLimitResults;
import org.nrg.xnat.utils.DateRange;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@RunWith(JUnit4.class)
public class RecentStudiesStudyDateRangeLimitStrategyTest {

    private final static Logger log = LoggerFactory.getLogger(RecentStudiesStudyDateRangeLimitStrategyTest.class);

    private StudyDateRangeLimitStrategy strategy;

    @Before
    public void setup() {
	strategy = new RecentStudiesStudyDateRangeLimitStrategy(3);
    }

    @Test
    public void limitStudyDateRangeCallersRangeIsSpecificEnough() {
	StudyDateRangeLimitResults results = strategy.limitStudyDateRange(buildSearchCriteriaFromStudyDateRange(
		new Date(), new Date()));
	assertFalse(results.isLimited());
    }

    @Test
    public void limitStudyDateRangeNoNeedBecuasePrimaryKeyFieldIsSpecified() {
	PacsSearchCriteria searchCriteria = new PacsSearchCriteria();
	searchCriteria.setPatientId("29572");
	assertFalse(strategy.limitStudyDateRange(searchCriteria).isLimited());
    }

    @Test
    public void limitStudyDateRangeRangeLimitInvoked() {
	Date oneWeekAgo = getDateFromDaysAgo(7);
	Date today = getDateFromDaysAgo(0);

	StudyDateRangeLimitResults results = strategy.limitStudyDateRange(buildSearchCriteriaFromStudyDateRange(
		oneWeekAgo, today));
	assertTrue(results.isLimited());
	Date threeDaysAgo = getDateFromDaysAgo(3);
	assertEquals(threeDaysAgo, results.getDateRange().getStart());
	assertEquals(today, results.getDateRange().getEnd());
	assertNotNull(results.getLimitExplanation());
	log.info(results.getLimitExplanation());
    }

    @Test
    public void limitStudyDateRangeNoRangeSpecifiedSoRangeLimitInvoked() {
	assertTrue(strategy.limitStudyDateRange(new PacsSearchCriteria()).isLimited());
    }

    private Date getDateFromDaysAgo(int x) {
	Calendar cal = Calendar.getInstance();
	cal.add(Calendar.DATE, -x);
	return DateUtils.truncate(cal.getTime(), Calendar.DATE);
    }

    private PacsSearchCriteria buildSearchCriteriaFromStudyDateRange(Date startRange, Date endRange) {
	PacsSearchCriteria searchCriteria = new PacsSearchCriteria();
	searchCriteria.setStudyDateRange(new DateRange(startRange, endRange));
	return searchCriteria;
    }
}
