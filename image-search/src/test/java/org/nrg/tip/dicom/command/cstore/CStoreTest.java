/*
 * org.nrg.tip.dicom.command.cstore.CStoreTest
 * TIP is developed by the Neuroinformatics Research Group
 * XNAT http://www.xnat.org
 * Copyright (c) 2013, Washington University School of Medicine
 * All Rights Reserved
 *
 * Released under the Simplified BSD.
 *
 * Last modified 9/24/13 6:11 PM
 */

package org.nrg.tip.dicom.command.cstore;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.nrg.tip.dicom.command.DicomCommandTest;
import org.springframework.core.io.ClassPathResource;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
public class CStoreTest extends DicomCommandTest {

    private CStoreSCU cstoreSCU;

    @Before
    public void before() {
	cstoreSCU = new BasicCStoreSCU(dicomConnectionProperties);
    }

    /**
     * Full-blown unit testing of this public interface is difficult as we'd need an XNAT set up. I'm exposing the
     * helper at package level so we can at least test the C-STORE operation using some canned files.
     * 
     * @return
     */
    @Test
    public void cstoreFiles() throws IOException {
	// test wiring
	ClassPathResource dicomFilesResource = new ClassPathResource("DICOM", getClass());
	File dicomFilesDir = dicomFilesResource.getFile();
	List<File> dicomFiles = Arrays.asList(dicomFilesDir.listFiles());
	((BasicCStoreSCU) cstoreSCU).cStoreFiles(dicomFiles);
    }
}
