/*
 * org.nrg.tip.dicom.strategy.orm.ResultSetLimitStrategyTest
 * TIP is developed by the Neuroinformatics Research Group
 * XNAT http://www.xnat.org
 * Copyright (c) 2013, Washington University School of Medicine
 * All Rights Reserved
 *
 * Released under the Simplified BSD.
 *
 * Last modified 9/24/13 6:11 PM
 */

package org.nrg.tip.dicom.strategy.orm;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.util.Date;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;
import org.nrg.tip.dto.PacsSearchCriteria;
import org.nrg.xnat.utils.DateRange;

@RunWith(JUnit4.class)
public class ResultSetLimitStrategyTest {

    private ResultSetLimitStrategy strategy;

    @Before
    public void setup() {
	strategy = new BasicResultSetLimitStrategy();
    }

    @Test
    public void patientLastNameIsSufficientlySpecificForSearchingEmpty() {
	assertFalse(strategy.searchCriteriaIsSufficientlySpecific(buildSearchCriteriaFromPatientName(null)));
    }

    @Test
    public void patientLastNameIsSufficientlySpecificForSearchingWildcardOnly() {
	assertFalse(strategy.searchCriteriaIsSufficientlySpecific(buildSearchCriteriaFromPatientName("*")));
    }

    @Test
    public void patientLastNameIsSufficientlySpecificForSearchingCommaPlusFirstName() {
	assertFalse(strategy.searchCriteriaIsSufficientlySpecific(buildSearchCriteriaFromPatientName(",Eric")));
    }

    @Test
    public void patientLastNameIsSufficientlySpecificForSearchingMiddleNameOnly() {
	assertFalse(strategy.searchCriteriaIsSufficientlySpecific(buildSearchCriteriaFromPatientName(",,dd")));
    }

    @Test
    public void patientLastNameIsSufficientlySpecificForSearchingSingleLetterOfLastName() {
	assertTrue(strategy.searchCriteriaIsSufficientlySpecific(buildSearchCriteriaFromPatientName("C")));
    }

    @Test
    public void studyDateIsSufficientlySpecificForSearchingUnboundedRange() {
	assertFalse(strategy.searchCriteriaIsSufficientlySpecific(buildSearchCriteriaFromStudyDateRange()));
    }

    @Test
    public void studyDateIsSufficientlySpecificForSearchingBoundedRange() {
	assertTrue(strategy.searchCriteriaIsSufficientlySpecific(buildSearchCriteriaFromStudyDateRange(new Date(),
		new Date())));
    }

    private PacsSearchCriteria buildSearchCriteriaFromPatientName(String patientName) {
	PacsSearchCriteria searchCriteria = new PacsSearchCriteria();
	searchCriteria.setPatientName(patientName);
	return searchCriteria;
    }

    private PacsSearchCriteria buildSearchCriteriaFromStudyDateRange() {
	return buildSearchCriteriaFromStudyDateRange(null, null);
    }

    private PacsSearchCriteria buildSearchCriteriaFromStudyDateRange(Date start, Date end) {
	PacsSearchCriteria searchCriteria = new PacsSearchCriteria();
	searchCriteria.setStudyDateRange(new DateRange(start, end));
	return searchCriteria;
    }
}
