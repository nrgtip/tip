/*
 * org.nrg.tip.dicom.command.cmove.CMoveTest
 * TIP is developed by the Neuroinformatics Research Group
 * XNAT http://www.xnat.org
 * Copyright (c) 2013, Washington University School of Medicine
 * All Rights Reserved
 *
 * Released under the Simplified BSD.
 *
 * Last modified 9/24/13 6:11 PM
 */

package org.nrg.tip.dicom.command.cmove;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.nrg.tip.dicom.command.DicomCommandTest;
import org.nrg.tip.dicom.command.cfind.SearchCriteriaTooVagueException;
import org.nrg.tip.dicom.command.cmove.dcm4che.tool.Dcm4cheToolCMoveSCU;
import org.nrg.tip.domain.Series;
import org.nrg.tip.domain.Study;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
public class CMoveTest extends DicomCommandTest {

    private CMoveSCU cmoveSCU;

    @Before
    public void before() {
        cmoveSCU = new Dcm4cheToolCMoveSCU(dicomConnectionProperties, ormStrategy);
    }

    @Test(expected = SearchCriteriaTooVagueException.class)
    public void cmoveSeriesEmptySearchCriteria() {
        cmoveSCU.cmoveSeries(new Study(), new Series());
    }

    @Test
    public void cmoveSeries() {
        Study study = new Study("1.2.840.113654.2.45.2.108105");
        Series series = new Series("1.2.840.113654.2.45.2.115982");
        try {
            cmoveSCU.cmoveSeries(study, series);
        } catch (CMoveFailureException e) {
            String additionalExplanation = "This unit test requires that your local XNAT development instance is up and listening as a DICOM C-STORE SCP.\nIt also requires that the PACS knows about your development instance (either has it in its AET whitelist, or is operating in promiscuous mode).\nCheck those items as potential causes of this failure.";
            throw new RuntimeException(additionalExplanation, e);
        }
    }

    @Test(expected = CMoveTargetNotFoundException.class)
    public void cmoveSeriesNotFound() {
        Study study = new Study("3456363634");
        Series series = new Series("2435asdf");
        series.setStudy(study);
        cmoveSCU.cmoveSeries(study, series);
    }

    @Test(expected = SearchCriteriaTooVagueException.class)
    public void cmoveSeriesNull() {
        cmoveSCU.cmoveSeries(null, null);
    }
}
