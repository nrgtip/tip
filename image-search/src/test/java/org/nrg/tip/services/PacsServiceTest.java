/*
 * org.nrg.tip.services.PacsServiceTest
 * TIP is developed by the Neuroinformatics Research Group
 * XNAT http://www.xnat.org
 * Copyright (c) 2013, Washington University School of Medicine
 * All Rights Reserved
 *
 * Released under the Simplified BSD.
 *
 * Last modified 9/24/13 6:11 PM
 */

package org.nrg.tip.services;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.nrg.tip.TipBaseTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import javax.inject.Inject;

import static org.junit.Assert.assertNotNull;

@RunWith(SpringJUnit4ClassRunner.class)
public class PacsServiceTest extends TipBaseTest {

    @Inject
    private PacsService pacsService;

    @Test
    public void testWiring() {
	assertNotNull(pacsService);
    }

    @Test
    public void testPacsManagement() {
    }
}
