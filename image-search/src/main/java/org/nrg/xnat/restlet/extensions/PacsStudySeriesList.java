/*
 * org.nrg.xnat.restlet.extensions.PacsStudySeriesList
 * TIP is developed by the Neuroinformatics Research Group
 * XNAT http://www.xnat.org
 * Copyright (c) 2013, Washington University School of Medicine
 * All Rights Reserved
 *
 * Released under the Simplified BSD.
 *
 * Last modified 9/24/13 6:11 PM
 */

package org.nrg.xnat.restlet.extensions;

import org.nrg.tip.domain.Series;
import org.nrg.tip.domain.Study;
import org.nrg.tip.dto.PacsSearchResults;
import org.nrg.tip.restlet.JsonViews;
import org.nrg.xdat.XDAT;
import org.nrg.xnat.restlet.XnatRestlet;
import org.restlet.Context;
import org.restlet.data.Request;
import org.restlet.data.Response;
import org.restlet.resource.Representation;
import org.restlet.resource.ResourceException;
import org.restlet.resource.Variant;

@XnatRestlet("/services/pacs/{PACS_ID}/search/studies/{STUDY_ID}/series")
public class PacsStudySeriesList extends PacsServiceResource {

    public PacsStudySeriesList(final Context context, final Request request, final Response response) {
        super(context, request, response);
    }

    @Override
    public Representation represent(final Variant variant) throws ResourceException {
        try {
            final Study study = buildStudyFromRequest();
            final PacsSearchResults<String, Series> series = getPacsService().getSeriesByStudy(XDAT.getUserDetails(),
                    getPacs(), study);
            if (0 == series.getResults().size()) {
                respondWithNotFound();
                return null;
            } else {
                return jsonRepresentation(series, JsonViews.SeriesRootView.class);
            }
        } catch (final PacsNotFoundException e) {
            respondWithPacsNotFound();
            return null;
        }
    }

    private Study buildStudyFromRequest() {
        final Study study = new Study();
        study.setStudyInstanceUid((String) getParameter(getRequest(), "STUDY_ID"));
        return study;
    }
}
