/*
 * org.nrg.tip.services.PacsEntityService
 * TIP is developed by the Neuroinformatics Research Group
 * XNAT http://www.xnat.org
 * Copyright (c) 2013, Washington University School of Medicine
 * All Rights Reserved
 *
 * Released under the Simplified BSD.
 *
 * Last modified 9/24/13 6:11 PM
 */

package org.nrg.tip.services;

import org.nrg.framework.orm.hibernate.BaseHibernateService;
import org.nrg.tip.domain.entities.Pacs;

public interface PacsEntityService extends BaseHibernateService<Pacs> {
}
