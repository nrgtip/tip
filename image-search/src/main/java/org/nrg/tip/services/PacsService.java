/*
 * org.nrg.tip.services.PacsService
 * TIP is developed by the Neuroinformatics Research Group
 * XNAT http://www.xnat.org
 * Copyright (c) 2013, Washington University School of Medicine
 * All Rights Reserved
 *
 * Released under the Simplified BSD.
 *
 * Last modified 9/24/13 6:11 PM
 */

package org.nrg.tip.services;

import org.nrg.tip.domain.Patient;
import org.nrg.tip.domain.Series;
import org.nrg.tip.domain.Study;
import org.nrg.tip.domain.entities.Pacs;
import org.nrg.tip.dto.PacsSearchCriteria;
import org.nrg.tip.dto.PacsSearchResults;
import org.nrg.xdat.om.XnatImagescandata;
import org.nrg.xdat.security.XDATUser;

public interface PacsService {

    PacsSearchResults<String, Patient> getPatientsByExample(XDATUser user, final Pacs pacs,
                                                            final PacsSearchCriteria searchCriteria);

    Patient getPatientById(XDATUser user, final Pacs pacs, String patientId);

    PacsSearchResults<String, Study> getStudiesByExample(XDATUser user, final Pacs pacs,
                                                         final PacsSearchCriteria searchCriteria);

    Study getStudyById(XDATUser user, final Pacs pacs, final String studyInstanceUid);

    PacsSearchResults<String, Series> getSeriesByStudy(XDATUser user, final Pacs pacs, final Study study);

    Series getSeriesById(XDATUser user, final Pacs pacs, final String seriesInstanceUid);

    void importSeries(XDATUser user, final Pacs pacs, final Study study, final Series series);

    void exportSeries(XDATUser user, final Pacs pacs, final XnatImagescandata series);
}
