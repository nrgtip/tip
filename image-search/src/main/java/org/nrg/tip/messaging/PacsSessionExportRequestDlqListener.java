/*
 * org.nrg.tip.messaging.PacsSessionExportRequestDlqListener
 * TIP is developed by the Neuroinformatics Research Group
 * XNAT http://www.xnat.org
 * Copyright (c) 2013, Washington University School of Medicine
 * All Rights Reserved
 *
 * Released under the Simplified BSD.
 *
 * Last modified 9/24/13 6:11 PM
 */

package org.nrg.tip.messaging;

import org.nrg.xdat.XDAT;
import org.nrg.xdat.security.XDATUser;
import org.nrg.xdat.security.XDATUser.UserNotFoundException;
import org.nrg.xft.XFT;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class PacsSessionExportRequestDlqListener {

    private final static Logger log = LoggerFactory.getLogger(PacsSessionExportRequestDlqListener.class);

    public void onPacsSessionExportRequest(final PacsSessionExportRequest pacsSessionExportRequest) throws Exception {
        try {
            log.info("DLQ listener received session export request");
            sendFailureNotification(pacsSessionExportRequest);
            log.info("DLQ listener completed session export request");
        } catch (final Exception e) {
            // If errors are not logged before they're rethrown, they do not show up in any of the files
            log.error("Choked on request " + pacsSessionExportRequest + " with the following error:\n" + e);
            throw e;
        }
    }

    private void sendFailureNotification(final PacsSessionExportRequest pacsSessionExportRequest) throws Exception {
        // refresh the user, just in case their email has changed since they made the request
        try {
            final XDATUser user = new XDATUser(pacsSessionExportRequest.getRequestingUser().getLogin());
            XDAT.getMailService()
                    .sendMessage(XFT.GetAdminEmail(), new String[]{
                            user.getEmail()
                    }, new String[]{
                            XFT.GetAdminEmail()
                    }, "PACS Session Export Request FAILED",
                            "Sorry!  The system was unable to export the study you requested from TIP to the PACS.  We're looking into it...");
        } catch (final UserNotFoundException e) {
            // not much to do here - was their account deleted since they made the request?
            log.error(
                    String.format("User %s queued up a PACS import request, but their user account cannot be found to send them a failure email."),
                    pacsSessionExportRequest.getRequestingUser().getLogin());
        }
    }
}
