/*
 * org.nrg.tip.dicom.strategy.orm.ResultSetLimitStrategy
 * TIP is developed by the Neuroinformatics Research Group
 * XNAT http://www.xnat.org
 * Copyright (c) 2013, Washington University School of Medicine
 * All Rights Reserved
 *
 * Released under the Simplified BSD.
 *
 * Last modified 9/24/13 6:11 PM
 */

package org.nrg.tip.dicom.strategy.orm;

import org.dcm4che2.tool.dcmqr.DcmQR.QueryRetrieveLevel;
import org.nrg.tip.dto.PacsSearchCriteria;
import org.nrg.tip.dto.StudyDateRangeLimitResults;

public interface ResultSetLimitStrategy {

    boolean searchCriteriaIsSufficientlySpecific(PacsSearchCriteria searchCriteria);

    int getMaxResultsForQueryLevel(QueryRetrieveLevel level);

    StudyDateRangeLimitResults limitStudyDateRange(PacsSearchCriteria searchCriteria);
}
