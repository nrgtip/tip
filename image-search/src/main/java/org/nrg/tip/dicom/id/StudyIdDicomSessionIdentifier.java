/*
 * org.nrg.tip.dicom.id.StudyIdDicomSessionIdentifier
 * TIP is developed by the Neuroinformatics Research Group
 * XNAT http://www.xnat.org
 * Copyright (c) 2013, Washington University School of Medicine
 * All Rights Reserved
 *
 * Released under the Simplified BSD.
 *
 * Last modified 9/24/13 6:11 PM
 */
package org.nrg.tip.dicom.id;

import java.util.List;

import org.dcm4che2.data.Tag;
import org.nrg.dcm.Extractor;
import org.nrg.dcm.TextExtractor;

import com.google.common.collect.ImmutableList;

public class StudyIdDicomSessionIdentifier {
    private static final ImmutableList<Extractor> sessionExtractors;

    static {
        final ImmutableList.Builder<Extractor> sessb = new ImmutableList.Builder<Extractor>();
        sessb.add(new TextExtractor(Tag.StudyID));
        sessionExtractors = sessb.build();
    }

    public static final List<Extractor> getSessionExtractors() {
        return sessionExtractors;
    }

    private StudyIdDicomSessionIdentifier() {
    }
}
